import time
import string
import random
from collections import defaultdict
from functools import wraps
import math
import sys
from itertools import izip, count
from optparse import OptionParser




# http://leadsift.com/loop-map-list-comprehension/
# http://pycallgraph.slowchop.com/en/master/guide/command_line_usage.html#command-line-usage
"""
O(1) -> O(lg n) -> O(n lg n) -> O(n^2) -> O(n^3) -> O(n^k) -> O(k^n) -> O(n!)
"""
def parse_input():
    parser = OptionParser()

    parser.add_option("-i", "--iterations",
                      action="store",  # optional because action defaults to "store"
                      dest="iterations",
                      default=_ITERATION,
                      help="Number of iterations",)

    (options, args) = parser.parse_args()
    return int(options.iterations)


def splitting_results():
    splitted_results = dict()
    for k in _results:
        splitted_k = k.split('_')[0]
        try:
            splitted_results[splitted_k].append((k, _results[k]))
        except:
            splitted_results[splitted_k] = list()
            splitted_results[splitted_k].append((k, _results[k]))
    return splitted_results


def plot(size, iterations, splitted_results, only=None):
    import matplotlib.pyplot as plt
    for k in splitted_results:
        fig = plt.figure()
        plt.ylabel('[ms]')
        for r in splitted_results[k]:
            label = r[0]
            values = r[1]
            x = xrange(len(values))
            plt.plot(x, values, label=label)
            plt.legend(loc='best')
        plt.savefig(k + '_' + str(size) + '_' + str(iterations) + '.png')
        plt.close(fig)



def timeit(method):

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        e = (te - ts) * 1000

        # print '{:20} {}'.format(method.__name__, e)
        try:
            _results[method.__name__].append(e)
        except KeyError:
            _results[method.__name__] = list()
            _results[method.__name__].append(e)

        return result, method.__name__, e

    return timed


def random_char(y):
    return ''.join(random.choice(string.ascii_letters) for x in range(y))


def double(x):
    return 2 * x


# ====== decorator memo

def memo(f):
    cache = dict()

    @wraps(f)
    def wrap(*arg):
        if arg not in cache:
            cache[arg] = f(*arg)
        return cache[arg]
    return wrap

# ====== iterate lists


@timeit
def lists_izip(input_list_int, input_list_str):
    for i, x, y in izip(count(), input_list_int, input_list_str):
        pass


@timeit
def lists_enum(input_list_int, input_list_str):
    for i, (x, y) in enumerate(zip(input_list_int, input_list_str)):
        pass

# ====== assign


@timeit
def assign_swap(input_list):
    x, y = 0, 1
    for i in input_list:
        x, y = y, x


@timeit
def assign_tmp(input_list):
    x, y = 0, 1
    for i in input_list:
        tmp = x
        x = y
        y = tmp


# ====== re-map

class Test:
    def check(self, a, b, c):
        if a == 0:
            self.str = b * 100
        else:
            self.str = c * 100


@timeit
def remap_off(input_list):
    a = Test()
    for i in input_list:
        a.check(i, "b", "c")


class Test2:
    def check(self, a, b, c):
        self.str = b * 100
        self.check = self.check_post

    def check_post(self, a, b, c):
        self.str = c * 100


@timeit
def remap_on(input_list):
    a = Test2()
    for i in input_list:
        a.check(i, "b", "c")


# ====== init dict

@timeit
def dict_if(input_list):
    wdict = dict()
    for word in input_list:
        if word not in wdict:
            wdict[word] = 0
        wdict[word] += 1


@timeit
def dict_try(input_list):
    wdict = dict()
    for word in input_list:
        try:
            wdict[word] += 1
        except KeyError:
            wdict[word] = 1


@timeit
def dict_get(input_list):
    wdict = dict()
    for word in input_list:
        wdict[word] = wdict.get(word, 0) + 1


@timeit
def dict_setd(input_list):
    wdict = dict()
    for word in input_list:
        wdict.setdefault(word, 0)
        wdict[word] += 1


@timeit
def dict_default(input_list):
    wdict = defaultdict(int)

    for word in input_list:
        wdict[word] += 1

# ====== dots


@timeit
def dot_on(input_list):
    output_list = list()
    for i in input_list:
        output_list.append(double(i))
    return output_list


@timeit
def dot_off(input_list):
    output_list = list()
    append = output_list.append
    for i in input_list:
        append(double(i))
    return output_list

# ====== STRING CONCAT


@timeit
def str_plus(input_list):
    s = str()
    for i in input_list:
        s += i
    return s


@timeit
def str_join(input_list):
    return ''.join(input_list)


# ====== LOOP =====

@timeit
def loop_for(input_list):
    output_list = list()
    for i in input_list:
        output_list.append(double(i))
    return output_list


@timeit
def loop_map(input_list):
    return map(double, input_list)


@timeit
def loop_comprehension(input_list):
    return [double(x) for x in input_list]


def run(size):
    input_list_int = xrange(0, size, 2)
    input_list_str = random_char(size)
    loop_for(input_list_int)
    loop_map(input_list_int)
    loop_comprehension(input_list_int)
    str_plus(input_list_str)
    str_join(input_list_str)
    dot_on(input_list_int)
    dot_off(input_list_int)
    dict_if(input_list_str)
    dict_try(input_list_str)
    dict_get(input_list_str)
    dict_setd(input_list_str)
    dict_default(input_list_str)
    remap_off(input_list_str)
    remap_on(input_list_str)
    assign_tmp(input_list_int)
    assign_swap(input_list_int)
    lists_izip(input_list_int, input_list_str)
    lists_enum(input_list_int, input_list_str)


def print_result(splitted_results, iterations):
    print 'Iterations:', iterations
    formatted = '{0:20} {1:20}'
    print (formatted.format('Method', 'Average [ms]'))
    for k in splitted_results:
        for r in splitted_results[k]:
            label = r[0]
            values = r[1]
            avg = sum(values) / len(values)
            print (formatted.format(label, str(avg)))


_SIZE = [100000]
_ITERATION = 1000
_results = dict()

if __name__ == '__main__':
    iterations = parse_input()
    for s in _SIZE:
        _results.clear()
        for i in xrange(iterations):
            sys.stdout.write("Iteration: %d  \r" % (i + 1))
            sys.stdout.flush()
            run(s)
        split_results = splitting_results()

        print_result(split_results, iterations)
        plot(s, iterations, split_results)
