
# http://www.techbeamers.com/python-programming-mistakes/
# =====  class variable


class A(object):
    x = 1


class B(A):
    pass


class C(A):
    pass


def class_variables():
    print(A.x, B.x, C.x)
    B.x = 2
    print(A.x, B.x, C.x)
    A.x = 3
    print(A.x, B.x, C.x)



# =====  default value
"""
the default value for a function argument is only evaluated once,
at the time that the function is defined.
"""


def foo_wrong(bar=list()):
    bar.append('baz')
    return bar


def foo_right(bar=None):
    if bar is None:
        bar = list()
    bar.append('baz')
    return bar


def default_value():
    foo_wrong()
    foo_wrong()
    print foo_wrong()
    foo_right()
    foo_right()
    print foo_right()


if __name__ == "__main__":
    default_value()
    class_variables()
